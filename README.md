# Tutorias Programación web 2021 C2 

En este repo compartiremos los ejercicios de la clase de programación web ciclo 2 del 2021

## Sesiones

* Tutoria 1: Nos familiarizamos con el entorno de desarrollo
* Tutoria 2: Resolvimos dudas de la guía y realizamos ejercicios de desarrollo de aplicaciones web de manera colaborativa
