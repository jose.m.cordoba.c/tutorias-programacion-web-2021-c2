# Ejercicio 3

Generar como equipo de scrum los resultados para nuestra primer iteracion

## Scrum planing

Maximo 30min para iteraciones de 3d

Realizar la reunion de planeación de la iteración

* Cuales HU ingresan en esta iteración?
* Cual es esfuerzo por parte del equipo para resolver estas HU?

## Stand up meeting (diaria)

* Que hice ayer?
* Que tengo planeado para hoy?
* Tengo algun bloqueante?

## Scrum retrospective

Maximo 30m

Al finalizar la iteración

* Que salió bien?
* Que no salio bien?
* Que hay que empezar a hacer?
* Que hay que seguir haciendo?
* Que hay que hacer menos?
* Cual es la velocidad del equipo?

## Ejercicio

* Unirse al proyecto de Gitlab y esperar a tener una HU asignada
* Crear una rama "feature/NOMBRE_DE_HU
* Mover en el tablero de Ejercico 3 la HU a doing
* Desarrollar la caracteristica
* Hacer un MR a master 
* Mover en el tablero la HU a Merge Request
* Un compañero probará la caracteristica
* Mover a testing la HU
* Una vez este validada aprobar la caracteristica
* El Docente fusiona la caracteristica

