enlistarPKM('https://pokeapi.co/api/v2/pokemon');
function enlistarPKM(apiUrl) {
    fetch(apiUrl)
    .then(response => response.json())
    .then(data => {
        // Obtenemos nuestros pokemones
        let pkms = data.results;

        // Obtenemos el elemento del DOM
        let pkmContainer = document.getElementById('pkm_list');
        pkmContainer.innerHTML ='';
        //console.log(pkms);
        pkms.forEach(pkm => {
            let name = pkm.name;
            let html_element = document.createElement('li'); //se crea un elemento li
            let a_element = document.createElement('a'); // se crea un elemento a
            a_element.innerText = name; //se agrega el nombre del pokemon al elemento a
            a_element.setAttribute("href", "#"); //se asigna un atributo al elemento a
            a_element.setAttribute("onclick", `dibujarPkm('${pkm.url}')`); //se asigna un atributo onclick con la funcion dibujarPkm() al elemento a
            html_element.append(a_element); //se agrega el enlace a la lista
            pkmContainer.append(html_element); //se agrega el elemento li a la lista
        });
        let linkIzquierda = document.getElementById('previous');
        let linkDerecha = document.getElementById('next');
        linkIzquierda.setAttribute("onclick", `enlistarPKM('${data.previous}')`);
        linkDerecha.setAttribute("onclick", `enlistarPKM('${data.next}')`);
    });

    
}

function dibujarPkm(urlApiPkm) {
    fetch(urlApiPkm)
    .then(response => response.json())
    .then(data => {
        let nombre = data.name;
        let imagen = data.sprites.front_default;
        let peso = data.weight;
        let altura = data.height;
        document.getElementById("nombrePKM").innerText = nombre;
        document.getElementById("alturaPKM").innerText = altura;
        document.getElementById("pesoPKM").innerText = peso;
        document.getElementById("imgPkm").setAttribute("src", imagen);
    });
}